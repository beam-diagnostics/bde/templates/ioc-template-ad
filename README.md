# ioc-template-ad

IOC template for areaDetector based IOCs.

## Usage

Create a dedicated empty git repository and copy the files and folders from this git repository over.

## iocApp/Db/Makefile

EPICS database files to be installed from `iocApp/Db/`.


## iocApp/src/Makefile

Specify detector DBD and libraries used during IOC build.

## iocBoot/ content

IOC instances are folders under `iocBoot` folder. Each instance needs to have a `st.cmd` file.

Example IOC instance called _xyz_ : `iocBoot/xyz/st.cmd`

## iocBoot/iocsh/ content

Snippets go into `iocBoot/iocsh/`.

