< envVars

errlogInit(20000)

#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase) 

#- 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("LOCATION",                     "")
epicsEnvSet("DEVICE_ID",                    "")
epicsEnvSet("DEVICE_NAME",                  "")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "")
epicsEnvSet("MAX_SAMPLES",                  "")
#- AD plugin macros
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

asynSetMinTimerPeriod(0.001)

#- Create a driver
#simDetectorConfig("$(PORT)", $(XSIZE), $(YSIZE), 1, 0, 0)
#dbLoadRecords("simDetector.template","P=$(PREFIX),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1")

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)


#- Create plugins


#- autosave
#- the path to be prepended to save-file and restore-file names
set_savefile_path("$(AUTOSAVE_DIR)")

#- required if using hand crafted request file
#set_requestfile_path("$(DB_DIR)")
#set_requestfile_path("./")

#- pass0: boot-time restore BEFORE record initialization; no record processing
#- pass1: boot-time restore AFTER record initialization; no record processing

#- load the PV defaults (located in IOC_DIR)
#set_pass0_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")
#set_pass1_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")

#- load makeAutosaveFiles() built request files
set_pass0_restoreFile("info_positions.sav")
set_pass1_restoreFile("info_settings.sav")

#- load hand crafted request files
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")

#- prefix for save_restore status PVs (see save_restoreStatus.db)
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("save_restoreStatus.db","P=$(PREFIX)")


###############################################################################
iocInit
###############################################################################

#- hand crafted request file, save things every thirty seconds
#create_monitor_set("auto_settings.req",30,"P=$(PREFIX),R=")

#- build info_positions.req file from record info 'autosaveFields_pass0' field
#- build info_settings.req file from record info 'autosaveFields' field
makeAutosaveFiles("")
create_monitor_set("info_positions.req", 5)
#- save things every thirty seconds
create_monitor_set("info_settings.req", 30)

date
###############################################################################

